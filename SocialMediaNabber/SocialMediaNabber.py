from InstagramAPI import InstagramAPI
from textblob import TextBlob
import time


class SocialMediaNabber:

    def __init__(self, login, password):
        self.api = InstagramAPI(login, password)
        self.api.login()

    def _get_userid(self, username):
        self.api.searchUsername(username)
        return self.api.LastJson['user']['pk']

    def _all_posts_gen(self, username):
        user_id = self._get_userid(username)
        next_max_post_id = ''

        while True:
            self.api.getUserFeed(user_id, next_max_post_id)
            items = self.api.LastJson['items']

            if len(items) == 0:
                break

            next_max_post_id = self.api.LastJson['next_max_id']
            for post in items:
                yield post, self._all_comments_for_post_gen(post['id'])
                time.sleep(5)

    def _all_comments_for_post_gen(self, post_id):
        next_max_comment_id = ''

        while True:
            self.api.getMediaComments(post_id, next_max_comment_id)
            comments = self.api.LastJson['comments']

            if len(comments) == 0:
                break

            next_max_comment_id = self.api.LastJson['next_max_id']

            for comment in comments:
                yield comment
                time.sleep(1)

    def print_all(self, username):
        for post, comments in self._all_posts_gen(username):
            print(f"""Post = {post['caption']['text']}
Comments={post['comment_count']}, Likes={post['like_count']}""")
            for comment in comments:
                text = TextBlob(comment['text'])
                sentiment = text.sentiment;
                print(f"{sentiment}, {comment['text']}\n")

if '__main__' == __name__:
    smn = SocialMediaNabber('username', 'password')
    smn.print_all('kourtneykardash')
